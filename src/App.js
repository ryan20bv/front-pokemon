import React from "react";

import { Provider } from "react-redux";
import indexStore from "./reduxToolKit/store/indexStore";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";

// import NavBar from "./components/layout/NavBar";
import Layout from "./components/layout/Layout";
// import DisplayArea from "./components/util/DisplayArea";
// import Home from "./components/pages/Home";
// import Details from "./components/pages/Details";
// import Pagination from "./components/util/Pagination";
import Loading from "./components/util/Loading";
// import ErrorPage from "./components/util/ErrorPage";

const NavBar = React.lazy(() => import("./components/layout/NavBar"));
// const Layout = React.lazy(() => import("./components/layout/Layout"));
const DisplayArea = React.lazy(() => import("./components/util/DisplayArea"));
const Home = React.lazy(() => import("./components/pages/Home"));
const Details = React.lazy(() => import("./components/pages/Details"));
const Pagination = React.lazy(() => import("./components/util/Pagination"));
// const Loading = React.lazy(() => import("./components/util/Loading"));
const ErrorPage = React.lazy(() => import("./components/util/ErrorPage"));

const App = () => {
	return (
		<Provider store={indexStore}>
			<BrowserRouter>
				<Layout>
					<React.Suspense fallback={<Loading />}>
						<NavBar />
						<DisplayArea>
							<Routes>
								{/* <Route
									path='/'
									element={<Navigate to='/home' replace={true} />}
								/> */}
								<Route path='/home' element={<Home />}>
									<Route path='' element={<Pagination />} />
								</Route>
								<Route
									path='/details'
									element={<Navigate to='/details/pikachu' replace={true} />}
								/>
								<Route path='/details/:pokemon_name' element={<Details />} />

								<Route path='/loading' element={<Loading />} />
								<Route path='/error' element={<ErrorPage />} />
								<Route
									path='*'
									element={<Navigate to='/home' replace={true} />}
								/>
							</Routes>
						</DisplayArea>
					</React.Suspense>
				</Layout>
			</BrowserRouter>
		</Provider>
	);
};

export default App;
