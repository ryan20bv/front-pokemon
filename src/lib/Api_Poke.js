export const getPokemonByBatch = async (url) => {
	let response = await fetch(url);
	if (!response.ok) {
		throw new Error("Network problem!");
	}

	const data = await response.json();
	// console.log(data);
	const mapItem = data.results.map((item) => {
		const data = getTypeApi(item);
		return data;
	});

	// console.log(mapItem);
	let promise;

	promise = await Promise.all(mapItem);

	return promise;
};

const getPokemonInfo = async (id) => {
	const response = await fetch(
		`https://pokeapi.co/api/v2/pokemon-species/${id}/`
	);

	const responseData = await response.json();

	if (!response.ok) {
		throw new Error("No Pokemon found by that name!");
	}

	const pokemonInfo = {
		text: responseData.flavor_text_entries[1].flavor_text,
		color: responseData.color.name,
		habitat: responseData.habitat?.name || "N/A",
	};
	return pokemonInfo;
};

export const getPokemonDetail = async (pokemonName) => {
	let responseData;
	try {
		const response = await fetch(
			`https://pokeapi.co/api/v2/pokemon/${pokemonName}`
		);

		responseData = await response.json();

		if (!response.ok) {
			throw new Error(`No Pokemon found by the name "${pokemonName}"!`);
		}
	} catch (err) {
		throw new Error(`No Pokemon found by the name "${pokemonName}"!`);
	}

	const ability_map = [];
	responseData.abilities.forEach((item) => {
		ability_map.push(item.ability.name);
	});

	const info = await getPokemonInfo(responseData.id);

	const pokemon_detail = {
		name: responseData.name,
		id: responseData.id,
		type: responseData.types[0].type.name,
		weight: responseData.weight,
		height: responseData.height,
		baseExperience: responseData.base_experience,
		ability: ability_map,
		picture: {
			front: responseData.sprites.front_default,
			back: responseData.sprites.back_default,
		},
		...info,
	};

	return pokemon_detail;
};

// export const promiseExecution = async (pokemon_name) => {
// 	let promise;

// 	promise = await Promise.all([
// 		getPokemonDetail(pokemon_name),
// 		// getPokemonInfo(),
// 	]);

// 	const pokemon_detail = {
// 		...promise[0],
// 		...promise[1],
// 	};

// 	return pokemon_detail;
// };

const getColorApi = async (pokemonName) => {
	let responseData;
	try {
		const response = await fetch(
			`https://pokeapi.co/api/v2/pokemon-species/${pokemonName}`
		);

		responseData = await response.json();
	} catch (err) {
		// console.log(err);
	}

	const colorName = responseData?.color?.name || "N/A";

	return colorName;
};

export const getTypeApi = async (item) => {
	const response = await fetch(item.url);
	const responseData = await response.json();

	const colorName = await getColorApi(item.name);
	const typeDetail = {
		name: responseData.name,
		type: responseData.types[0].type.name,
		image: responseData.sprites.front_default,
		id: responseData.id,
		color: colorName,
	};
	return typeDetail;
};
