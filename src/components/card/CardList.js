import React, { useEffect } from "react";

import { useSearchParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux/es/exports";

import { getBatchPokemon } from "../../reduxToolKit/action/pokemon-action";

import Loading from "../util/Loading";
import CardItem from "./CardItem";

import classes from "./cardList.module.css";

const CardList = ({ errorHandler }) => {
	const dispatch = useDispatch();
	const { fetchData, status, error } = useSelector(
		(state) => state.pokemonReducer
	);

	const [searchParams] = useSearchParams({});

	useEffect(() => {
		let getSearchParams = searchParams.get("set");
		let url = `https://pokeapi.co/api/v2/pokemon?offset=${getSearchParams}&limit=30`;
		if (!getSearchParams) {
			url = `https://pokeapi.co/api/v2/pokemon?offset=0&limit=30`;
		}

		dispatch(getBatchPokemon(url));
	}, [searchParams, dispatch]);

	if (status === "pending") {
		return (
			<div>
				<Loading />
			</div>
		);
	}

	if (error && status === "completed") {
		errorHandler(error);
	}

	let content;
	if (status === "completed" && !error) {
		content = fetchData.map((item) => {
			return <CardItem key={item.id} item={item} />;
		});
		return (
			<section>
				<div className={classes.cardListArea}>{content}</div>
			</section>
		);
	}
};

export default CardList;
