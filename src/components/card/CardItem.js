import React, { useEffect, useState } from "react";

import { useNavigate } from "react-router-dom";

import classes from "./cardItem.module.css";
import "../../index.css";
import pokeBall from "../../images/poke-ball3.png";

const CardItem = ({ item }) => {
	// console.log(item);
	const navigate = useNavigate();
	const [fetchImage, setFetchImage] = useState(pokeBall);
	const [isLoading, setIsLoading] = useState(true);
	const { name, type, image, id, color } = item;

	useEffect(() => {
		if (image) {
			setFetchImage(image);
			setIsLoading(false);
		}
	}, [image]);

	// const [type, setType] = useState();

	// const [color, setColor] = useState();
	// const [id, setId] = useState();

	// const { name, url } = item;
	/* 	useEffect(() => {
		setIsLoading(true);
		const getType = async () => {
			let response;
			try {
				response = await fetch(url);
				response = await response.json();
			} catch (err) {
				console.log("error type");
			}

			setType(response.types[0].type.name);
			setImage(response.sprites.front_default);
			setId(response.id);
			setIsLoading(false);
		};
		getType();
		const getColor = async () => {
			let responseData;
			try {
				const response = await fetch(
					`https://pokeapi.co/api/v2/pokemon-species/${name}`
				);

				responseData = await response.json();
			} catch (err) {}

			setColor(responseData?.color?.name || "N/A");
		};
		getColor();
	}, [name, url]); */

	const imageClass = isLoading ? classes.imageLoading : "";

	const nameClickHandler = (name) => {
		navigate(`/details/${name}`);
	};

	return (
		<div className={classes.card}>
			<div className={classes.image_holder}>
				<p>{id}</p>
				<img src={fetchImage} alt={name} className={imageClass} />
			</div>
			<header className={classes[color]}>
				<h3 onClick={() => nameClickHandler(name)}>{name}</h3>
				<p>({type})</p>
			</header>
		</div>
	);
};

export default CardItem;
