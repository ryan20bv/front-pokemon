import React, { useState, useEffect } from "react";

import { useSelector, useDispatch } from "react-redux/es/exports";
import { getDetail } from "../../reduxToolKit/action/pokemon-action";

import { useParams } from "react-router-dom";

import Loading from "../util/Loading";
import ErrorPage from "../util/ErrorPage";
import classes from "./detailCard.module.css";
const DetailCard = () => {
	const dispatch = useDispatch();
	const {
		detailStatus: status,
		error,
		pokemon_detail,
	} = useSelector((state) => state.pokemonReducer);

	const { pokemon_name } = useParams();

	const [mainImage, setMainImage] = useState("");
	// https://pokeapi.co/api/v2/pokemon/bulbasaur
	// https://pokeapi.co/api/v2/pokemon-species/1/
	const changeImageHandler = (img) => {
		setMainImage(img);
	};

	useEffect(() => {
		dispatch(getDetail(pokemon_name));
	}, [pokemon_name, dispatch]);

	useEffect(() => {
		if (pokemon_detail) {
			setMainImage(pokemon_detail?.picture?.front);
		}
	}, [pokemon_detail]);

	if (status === "pending") {
		return <Loading />;
	}
	if (error) {
		return <ErrorPage error={error} />;
	}

	if (status === "completed") {
		const {
			name,
			id,
			type,
			weight,
			height,
			baseExperience,
			ability,
			color,
			text,
			habitat,
		} = pokemon_detail;

		let backgroundColor = "";

		switch (color) {
			case "black":
				backgroundColor = "#a9a9a9";
				break;
			case "blue":
				backgroundColor = "#d1e4ff";
				break;
			case "brown":
				backgroundColor = "#eedd82";
				break;
			case "gray":
				backgroundColor = "#d3d3d3";
				break;
			case "green":
				backgroundColor = "#deffde";
				break;
			case "pink":
				backgroundColor = "#ffe4e1";
				break;
			case "purple":
				backgroundColor = "#c5badb";
				break;
			case "red":
				backgroundColor = "#ffd1d1";
				break;
			case "white":
				backgroundColor = "#ffffff";
				break;
			case "yellow":
				backgroundColor = "#fffacd";
				break;

			default:
				break;
		}

		return (
			<div className={classes.detailCard}>
				<section className={classes.section_image}>
					<div
						className={classes.main}
						style={{ backgroundColor: backgroundColor }}
					>
						<img src={mainImage} alt='bulbasaur' />
					</div>
					<div className={classes.thumbnails}>
						<img
							src={pokemon_detail?.picture?.front}
							alt={pokemon_name}
							onClick={() => changeImageHandler(pokemon_detail?.picture.front)}
						/>
						<img
							src={pokemon_detail?.picture?.back}
							alt={pokemon_name}
							onClick={() => changeImageHandler(pokemon_detail.picture.back)}
						/>
					</div>
				</section>
				<section
					className={classes.section_info}
					style={{ borderColor: backgroundColor }}
				>
					<table>
						<tbody>
							<tr>
								<th>Name:</th>
								<td>{name ? name : ""}</td>
								<th>Id:</th>
								<td>{id}</td>
							</tr>

							<tr>
								<th>Habitat:</th>
								<td>{habitat}</td>
								<th>Type:</th>
								<td>{type}</td>
							</tr>
							<tr>
								<th>Base Experience:</th>
								<td colSpan={3}>{baseExperience}</td>
							</tr>
							<tr>
								<th>Height:</th>
								<td>{height}</td>
								<th>Weight:</th>
								<td>{weight}</td>
							</tr>
							<tr>
								<th>Ability:</th>
								<td colSpan={3}>{ability.join(", ")}</td>
							</tr>
							<tr>
								<th>Text:</th>
								<td colSpan={3}>{text}</td>
							</tr>
						</tbody>
					</table>
				</section>
			</div>
		);
	}
};

export default DetailCard;
