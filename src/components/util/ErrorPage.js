import React from "react";
import classes from "./errorPage.module.css";

const ErrorPage = ({ error = "error" }) => {
	return <div className={classes.error}>{error}</div>;
};

export default ErrorPage;
