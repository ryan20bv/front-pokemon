import React, { useState, useRef, useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import classes from "./pagination.module.css";

const Pagination = () => {
	const [paginationScrollValue, setPaginationScrollValue] = useState(0);
	const [searchParams, setSearchParams] = useSearchParams({});
	const clickHandler = (e) => {
		setSearchParams({ set: e.target.name });
	};
	const pageRef = useRef();

	const queryParams = +searchParams.get("set");
	let content = [];

	let name = 0;
	let value = 1;
	for (let index = 0; index < 20; index++) {
		content.push({ name, value });
		name += 30;
		value++;
	}

	const scrollLeftHandler = () => {
		pageRef.current.scrollLeft -= 50;
		setPaginationScrollValue((pageRef.current.scrollLeft -= 50));
	};
	const scrollRightHandler = () => {
		pageRef.current.scrollLeft += 50;
		setPaginationScrollValue((pageRef.current.scrollLeft += 50));
	};
	useEffect(() => {
		pageRef.current.scrollLeft = paginationScrollValue;
	}, [paginationScrollValue]);

	return (
		<div className={classes.pagination}>
			<button onClick={scrollLeftHandler}>&laquo;</button>
			<div ref={pageRef}>
				{content.map((item, index) => {
					return (
						<button
							name={item.name}
							key={index}
							onClick={clickHandler}
							className={item.name === queryParams ? classes.active : " "}
						>
							{item.value}
						</button>
					);
				})}
			</div>

			<button onClick={scrollRightHandler}>&raquo;</button>
		</div>
	);
};

export default Pagination;
