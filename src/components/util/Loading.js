import React from "react";
import bulbasaur from "../../images/bulbasaur.jpg";
import charmander from "../../images/charmander.png";
import squirtle from "../../images/squirtle.png";
import pikachu from "../../images/pikachu.png";

import classes from "./loading.module.css";
const Loading = () => {
	return (
		<div className={classes.loading}>
			<div className={classes.image_holder}>
				<img src={bulbasaur} alt='bulbasaur' className={classes.bulbasaur} />
				<img src={charmander} alt='charmander' className={classes.charmander} />
				<img src={squirtle} alt='squirtle' className={classes.squirtle} />
				<img src={pikachu} alt='pikachu' className={classes.pikachu} />
			</div>
			<p>Loading...</p>
		</div>
	);
};

export default Loading;
