import React from "react";
import classes from "./displayArea.module.css";

const DisplayArea = (props) => {
	return <div className={classes.displayArea}>{props.children}</div>;
};

export default DisplayArea;
