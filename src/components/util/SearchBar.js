import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import classes from "./searchBar.module.css";

const SearchBar = () => {
	const navigate = useNavigate();
	const [inputValue, setInputValue] = useState("");
	const changeHandler = (e) => {
		setInputValue(e.target.value);
	};

	const submitHandler = (e) => {
		e.preventDefault();
		if (!inputValue && inputValue.length < 1) {
			return;
		}
		navigate(`/details/${inputValue.trim().toLowerCase()}`);
		setInputValue("");
	};

	return (
		<form className={classes.searchBar} onSubmit={submitHandler}>
			<input type='text' value={inputValue} onChange={changeHandler} />
			<button>Find Pokemon</button>
		</form>
	);
};

export default SearchBar;
