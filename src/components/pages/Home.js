import React, { useState, useCallback } from "react";
import { Outlet } from "react-router-dom";
import CardList from "../card/CardList";
import ErrorPage from "../util/ErrorPage";

const Home = () => {
	const [error, setError] = useState("");
	const errorHandler = useCallback((message) => {
		setError(message);
	}, []);
	if (error) {
		if (error) {
			return <ErrorPage error={error} />;
		}
	}
	return (
		<div>
			<Outlet />
			<CardList errorHandler={errorHandler} />
		</div>
	);
};

export default Home;
