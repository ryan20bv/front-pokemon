import React from "react";
import { NavLink } from "react-router-dom";
import classes from "./navLinks.module.css";
const NavLinks = () => {
	return (
		<div className={classes.navLinks}>
			<ul>
				<li>
					<NavLink
						to='/home'
						className={(navData) => (navData.isActive ? classes.active : "")}
					>
						{" "}
						Home
					</NavLink>
				</li>
				<li>
					<NavLink
						to='/details/'
						className={(navData) => (navData.isActive ? classes.active : "")}
					>
						Details
					</NavLink>
				</li>
			</ul>
		</div>
	);
};

export default NavLinks;
