import React from "react";

import MainNavigation from "./MainNavigation";
import SearchBar from "../util/SearchBar";

import classes from "./navBar.module.css";

const NavBar = () => {
	return (
		<div className={classes.navBar}>
			<MainNavigation />
			<SearchBar />
		</div>
	);
};

export default NavBar;
