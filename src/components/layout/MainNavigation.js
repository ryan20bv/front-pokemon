import React from "react";

import NavLinks from "./NavLinks";
import classes from "./mainNavigation.module.css";
import logo from "../../images/logo.png";

const MainNavigation = () => {
	return (
		<div className={classes.mainNav}>
			<img src={logo} alt='logo' />
			<NavLinks />
		</div>
	);
};

export default MainNavigation;
