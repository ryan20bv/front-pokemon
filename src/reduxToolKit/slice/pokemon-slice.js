import { createSlice } from "@reduxjs/toolkit";

const initialPokemonState = {
	status: "pending",
	fetchData: [],
	detailStatus: "pending",
	pokemon_detail: {},
	error: "",
};

const pokemonSlice = createSlice({
	name: "pokemon-slice",
	initialState: initialPokemonState,
	reducers: {
		changeStatus(state, action) {
			state.status = action.payload.status;
		},
		errorFetching(state, action) {
			state.error = action.payload.error;
		},
		updateFetchData(state, action) {
			state.fetchData = action.payload.fetchedData;
		},
		updateDetailStatus(state, action) {
			state.detailStatus = action.payload.detailStatus;
		},
		updateDetail(state, action) {
			state.pokemon_detail = action.payload.detail;
		},
	},
});

export default pokemonSlice;
