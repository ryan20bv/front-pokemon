import { configureStore } from "@reduxjs/toolkit";

import pokemonSlice from "../slice/pokemon-slice";

const indexStore = configureStore({
	reducer: {
		pokemonReducer: pokemonSlice.reducer,
	},
});

export default indexStore;
