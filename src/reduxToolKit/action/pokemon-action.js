import pokemonSlice from "../slice/pokemon-slice";
import { getPokemonByBatch, getPokemonDetail } from "../../lib/Api_Poke";

export const getBatchPokemon = (url) => async (dispatch) => {
	dispatch(
		pokemonSlice.actions.errorFetching({
			error: null,
		})
	);
	dispatch(
		pokemonSlice.actions.changeStatus({
			status: "pending",
		})
	);

	try {
		const results = await getPokemonByBatch(url);

		dispatch(
			pokemonSlice.actions.updateFetchData({
				fetchedData: results,
			})
		);
	} catch (err) {
		dispatch(
			pokemonSlice.actions.errorFetching({
				error: err.message,
			})
		);
	}
	dispatch(
		pokemonSlice.actions.updateDetailStatus({
			detailStatus: "pending",
		})
	);

	dispatch(
		pokemonSlice.actions.changeStatus({
			status: "completed",
		})
	);
};

export const getDetail = (pokemonName) => async (dispatch) => {
	dispatch(
		pokemonSlice.actions.errorFetching({
			error: null,
		})
	);
	dispatch(
		pokemonSlice.actions.changeStatus({
			status: "pending",
		})
	);
	dispatch(
		pokemonSlice.actions.updateDetailStatus({
			detailStatus: "pending",
		})
	);
	try {
		const results = await getPokemonDetail(pokemonName);

		dispatch(
			pokemonSlice.actions.updateDetail({
				detail: results,
			})
		);
	} catch (err) {
		dispatch(
			pokemonSlice.actions.errorFetching({
				error: err.message,
			})
		);
	}

	dispatch(
		pokemonSlice.actions.updateDetailStatus({
			detailStatus: "completed",
		})
	);
};
